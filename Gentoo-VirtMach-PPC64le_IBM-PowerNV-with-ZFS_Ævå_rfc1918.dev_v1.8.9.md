# Gentoo Linux on PPC64le - IBM PowerNV Architecture - ZFS
|Date      |Version| Author         |
|:---------|:------|:---------------|
|2023-10-22|1.8.9  |[Eva Winterschön](https://vernetzen.io/mastodon/profile-links.html)|

## The Installation Process
These stages will eventually be converted into an Ansible playbook.

### Reference Docs
- [Gentoo PPC64 Handbook](https://wiki.gentoo.org/wiki/Handbook:PPC64/Installation/About) 
- [Gentoo PPC64 Downloads](https://www.gentoo.org/downloads/#ppc)
- [Gentoo ZFS on Root EFI-Stub](https://wiki.gentoo.org/wiki/User:Ali3nx/Installing_Gentoo_Linux_EFISTUB_On_ZFS)
- [Gentoo ZFS on Root](https://wiki.gentoo.org/wiki/User:Fearedbliss/Installing_Gentoo_Linux_On_ZFS)

### Stage One - Bootstrapping -WIP WIP WIP WIP
```
export HOSTNAME="llm-vms0-ppc64-p9smt4n1"
hostname $HOSTNAME
apk add chimera-repo-contrib

echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
dinitctl start sshd
apk add bash bash-completion rsync wget gtar gsed
apk update && bash

unalias -a
alias l='/bin/ls -al'
alias ll='/bin/ls -al'
alias lh='/bin/ls -alh'

ssh-keygen -t ed25519 -f /root/.ssh/id_ed25519 -C "root@$HOSTNAME" -q -N ""
echo "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICGaGcRcmW6vcZqb3lqUHxoHdc5kLf3PKV3RdtBeZdYn eva@null.ed25519" > /root/.ssh/authorized_keys

# Wipe USB drive for /boot .. 16GB industrial tf-card is ideal
# ID can be obtained from /dev/disk-by-id
ZBOOT_ID="usb-Mass_Storage_Device_121220160204-0:0"
ZBOOT_DEV=$(realpath /dev/disk/by-id/${ZBOOT_ID})
ZBOOT_PART=$(echo ${ZBOOT_DEV}1)

cat<<EOF >/tmp/sfdisk.usb-boot.part
label: gpt
label-id: 95207A1F-6700-411F-9C9A-F729FF24AADB
device: /dev/sdd
unit: sectors
first-lba: 2048
last-lba: 31116254
sector-size: 512
/dev/sdd1 : start=        2048, size=     4194304, type=0FC63DAF-8483-4772-8E79-3D69D8477DE4, uuid=51C5C3F8-6CB6-4713-8663-A49C99059259, attrs="LegacyBIOSBootable"
/dev/sdd2 : start=     4196352, size=    26918912, type=0FC63DAF-8483-4772-8E79-3D69D8477DE4, uuid=4F553704-19E2-4BF9-BE1A-FF8603C91788
EOF

# write partitions and create boot filesystem
wipefs -a ${ZBOOT_DEV}
sfdisk ${ZBOOT_DEV} < /tmp/sfdisk.usb-boot.part
mkfs.ext4 -L ZFS_BOOT_EXT ${ZBOOT_PART}

# generate var for later use in grub
export ZBOOT_PART_UUID=$(blkid | grep ${ZBOOT_PART} | awk -F\" '{print $2}')

# Destroy SATADOM partitions for ZFS pool, matching specific device IDs
SDOMS="ata-SATADOM-SL_3IE3_V2_BCA11709200050517 ata-SATADOM-SL_3IE3_V2_BCA11708020382305 ata-SATADOM-SL_3IE3_V2_BCA11708020382932"

for sdom in ${SDOMS}; do dd if=/dev/zero of=$(realpath /dev/disk/by-id/${sdom}) count=512 bs=1M conv=sync status=progress; done
for sdom in ${SDOMS}; do wipefs -a -f $(realpath /dev/disk/by-id/${sdom}); done

## Optional, check for bad blocks
##  note: must create ext4 full disk before commands will work 
# » e2fsck -cfpv /dev/sdaN
# » e2fsck -cfpv /dev/sdbN
# » e2fsck -cfpv /dev/sdcN

# Initialize ZFS Pool ID [deadbeef]
mkdir /mnt/gentoo
zgenhostid -f -o /etc/hostid 0xd34db33f

# ------------------------------------------------------------------------------------------------- #
# ZFS w/ DRAID 
# ------------------------------------------------------------------------------------------------- #
zpool create -f -o ashift=12 -o cachefile=/etc/zfs/zpool.cache \
  -O compression=zstd -O xattr=sa -O relatime=on -O acltype=posixacl \
  -O dedup=off -m none -R /mnt/gentoo rpool draid1:1d:3c:1s \
  ata-SATADOM-SL_3IE3_V2_BCA11708020382305 \
  ata-SATADOM-SL_3IE3_V2_BCA11709200050517 \
  ata-SATADOM-SL_3IE3_V2_BCA11708020382932

# ------------------------------------------------------------------------------------------------- #
# ZFS w/ Mirror + Spare 
# ------------------------------------------------------------------------------------------------- #
# zpool create -f -o ashift=12 -o cachefile=/etc/zfs/zpool.cache \
#  -O compression=zstd -O xattr=sa -O relatime=on -O acltype=posixacl \
#  -O dedup=off -m none -R /mnt/gentoo rpool mirror ata-SATADOM-SL_3IE3_V2_BCA11708020382305 \
#  ata-SATADOM-SL_3IE3_V2_BCA11709200050517 spare ata-SATADOM-SL_3IE3_V2_BCA11708020382932

zfs create -o mountpoint=none -o canmount=off rpool/ROOT 
zfs create -o mountpoint=/ rpool/ROOT/gentoo

mkdir /mnt/gentoo/boot
mkdir /tmp/gentoo
mount ${ZBOOT_PART} /mnt/gentoo/boot

# STAGE3
BASEU="https://distfiles.gentoo.org"
BASED="releases/ppc/autobuilds/20231017T010159Z"
BASEN="stage3-power9le-musl-hardened-openrc-20231017T010159Z.tar.xz"
BASEX=('CONTENTS.gz' 'DIGESTS' 'asc' 'sha256')
URLG="${BASEU}/${BASED}/${BASEN}"
wget -O /tmp/gentoo/${BASEN} ${URLG}
wget -O /tmp/gentoo/${BASEN}.${BASEX[0]} ${URLG}/${BASEX[0]}
wget -O /tmp/gentoo/${BASEN}.${BASEX[1]} ${URLG}/${BASEX[1]}
wget -O /tmp/gentoo/${BASEN}.${BASEX[2]} ${URLG}/${BASEX[2]}
wget -O /tmp/gentoo/${BASEN}.${BASEX[3]} ${URLG}/${BASEX[3]}

cd /mnt/gentoo
gtar xJpvf /tmp/gentoo/stage3-*.tar.xz --xattrs-include='*.*' --numeric-owner

mkdir /mnt/gentoo/etc/zfs
cp /etc/zfs/zpool.cache /mnt/gentoo/etc/zfs/
cat /etc/resolv.conf > /mnt/gentoo/etc/resolv.conf

mount --types proc /proc /mnt/gentoo/proc
mount --rbind /sys /mnt/gentoo/sys
mount --make-rslave /mnt/gentoo/sys
mount --rbind /dev /mnt/gentoo/dev
mount --make-rslave /mnt/gentoo/dev
mount --bind /run /mnt/gentoo/run
mount --make-slave /mnt/gentoo/run

# Switch to CHROOT 
chroot /mnt/gentoo /bin/bash 
source /etc/profile 

# chroot usability
export PS1="(chroot) ${PS1}"
alias l='/bin/ls -al'
alias ll='/bin/ls -al'
alias lh='/bin/ls -alh'

# repo stuff
test -d || mkdir /etc/portage/repos.conf
mkdir -p /var/db/repos/gentoo
cp /usr/share/portage/config/repos.conf /etc/portage/repos.conf/gentoo.conf 

# package sync + build optimizations (ccache)
emerge-webrsync &&
  emerge app-text/ansifilter dev-util/ccache 

cat<<EOF > /etc/portage/make.conf
#~~CHOST="powerpc64le-unknown-linux-gnu"~~#
CHOST="powerpc64le-gentoo-linux-musl"
LC_MESSAGES=C.utf8
COMMON_FLAGS="-O2 -pipe -mcpu=power9 -mtune=power9"
CFLAGS="${COMMON_FLAGS}"
CXXFLAGS="${COMMON_FLAGS}"
FCFLAGS="${COMMON_FLAGS}"
FFLAGS="${COMMON_FLAGS}"
EMERGE_DEFAULT_OPTS="--with-bdeps y --complete-graph y"
BINPKG_COMPRESS="zstd"
BINPKG_COMPRESS_FLAGS="-9"
ACCEPT_LICENSE="*"
PORTAGE_LOG_FILTER_FILE_CMD="bash -c \"ansifilter; exec cat\""
PORTAGE_NICENESS="-15"
FEATURES="ccache"
CCACHE_DIR="/var/cache/ccache"
CCACHE_UMASK="0002"
USE="dist-kernel"
# END
EOF

# ---------------------------------------------------- #
# INITIAL Emerges
# ---------------------------------------------------- #
emerge eix
eix-update
emerge sys-apps/lm-sensors
sensors-detect --auto

# ---------------------------------------------------- #
# Custom Kernel Stuff
# ---------------------------------------------------- #
# commands for sys-kernel/gentoo-kernel
# echo "sys-kernel/gentoo-kernel debug hardened initramfs modules-sign savedconfig secureboot strip test" > /etc/portage/package.use/gentoo-kernel
# mkdir -p /etc/portage/savedconfig/sys-kernel/gentoo-kernel
# mkdir --parents /usr/src/initramfs/{bin,dev,etc,lib,lib64,mnt/root,proc,root,sbin,sys}
# cp -n "/usr/src/linux-$(uname -r)/.config" /etc/portage/savedconfig/sys-kernel/gentoo-kernel/
# emerge --ask --update --deep --with-bdeps=y --newuse sys-kernel/gentoo-sources

# enable USE=grub on sys-kernel/installkernel-gentoo to automatically run grub-mkconfig after upgrades
# It is possible to use a hook in /etc/kernel/install.d/ to automatically update other bootloaders or run arbitrary commands
# ---------------------------------------------------- #

emerge dracut bash-completion cronie chrony app-editors/vim  app-text/tree sys-apps/dbus sys-apps/haveged media-libs/fontconfig
emerge media-fonts/terminus-font media-fonts/corefonts
emerge gentoolkit gentoo-kernel-bin linux-firmware app-admin/sudo app-admin/doas

emerge sys-fs/zfs sys-fs/zfs-kmod
emerge --config gentoo-kernel-bin 

# ---------------------------------------------------- #
# BOOT Services, general
# ---------------------------------------------------- #
rc-update add chronyd boot
rc-update add cronie default
rc-update add dbus boot
rc-update add haveged default
rc-update add lm_sensors boot
rc-update add sshd default
rc-update add udev-settle sysinit
rc-update add zfs-import boot
rc-update add zfs-mount boot
rc-update add zfs-zed default

# ---------------------------------------------------- #
# BOOT Services, network
# ---------------------------------------------------- #
rc-update add net-online boot
rc-update add dhcpcd boot

cat<< EOF> /etc/conf.d/net-online
interfaces="enP4p1s0f0"
include_ping_test=yes
ping_test_host=9.9.9.9
timeout=60

# ---------------------------------------------------- #
# BOOT Services, ttys and hvc0
# ---------------------------------------------------- #
CDIR=$(pwd)
cd /etc/init.d
for i in {1..6}; do 
  ln -s agetty agetty.tty${i}; 
  rc-update add agetty.tty${i} default
done
ln -s agetty agetty.hvc0 && rc-update add agetty.hvc0
cd ${CDIR}
# ---------------------------------------------------- #

# ---------------------------------------------------- #
# OPEN-RC Settings
# ---------------------------------------------------- #
mv /etc/rc.conf /etc/rc.conf.dist
cat<< EOF> /etc/rc.conf
rc_hotplug="!net.wlan"
rc_interactive="YES"
rc_log_path="/var/log/rc.log"
rc_logger="YES"
rc_nocolor=NO
rc_nostop="agetty.hvc0"
rc_shell=/sbin/sulogin
rc_start_wait=100
rc_tty_number=12
rc_verbose=yes
unicode="YES"
EOF

# ---------------------------------------------------- #
# Set root password
# ---------------------------------------------------- #
passwd root

# ---------------------------------------------------- #
# ZFS HOST ID
# https://openzfs.github.io/openzfs-docs/man/master/8/zgenhostid.8.html
# ---------------------------------------------------- #
ZHOST_ID="0xd34db33f"
zgenhostid -f -o /etc/hostid ${ZHOST_ID}

# ---------------------------------------------------- #
# BOOT LOADER
# ---------------------------------------------------- #
# On PowerNV we don't need to setup the bootloader, 
#   but we need the config directory and application
#   note: for other PowerPC variants, like Apple G5, 
#   we'd install 'grub-powerpc-ieee1275' but we don't 
#   for the IBM/Raptor PowerNV
# ---------------------------------------------------- #
emerge sys-boot/grub

UUID_ZFS_RPOOL=$(blkid | grep "LABEL=\"rpool\"" | tail -n 1 | awk -F"UUID=" '{print $2}' | awk '{print $1}')
UUID_ZFS_BOOT_DEV=$(blkid | grep "LABEL=\"ZFS_BOOT_EXT\"" | tail -n 1 | awk -F"UUID=" '{print $2}' | awk '{print $1}')
UUID_ZFS_BOOT_PART=$(blkid | grep "LABEL=\"ZFS_BOOT_EXT\"" | tail -n 1 | awk -F"PARTUUID=" '{print $2}' | awk '{print $1}')
echo ${UUID_ZFS_RPOOL}
echo ${UUID_ZFS_BOOT_DEV}
echo ${UUID_ZFS_BOOT_PART}

mkdir /boot/grub
mv /etc/default/grub /etc/default/grub.dist

cat<<EOF > /etc/default/grub
# --------------------------------------------------------------------------- #
# GRUB2 Config
# Version: 1.85.2
# Date: 2023-10-20
# --------------------------------------------------------------------------- #
GRUB_DEFAULT=0
GRUB_TIMEOUT=5
# --------------------------------------------------------------------------- #
GRUB_FS="zfs"
GRUB_CMDLINE_LINUX_DEFAULT="loglevel=6 dozfs=force spl_hostid=${ZHOST_ID} mitigations=auto console_msg_format=syslog modprobe.blacklist=nouveau modprobe.blacklist=snd modprobe.blacklist=snd_hda_intel modprobe.blacklist=hci_vhci modprobe.blacklist=bluetooth modprobe.blacklist=cfg80211 hostname=llm-vms0-ppc64-p9smt4n1"
# --------------------------------------------------------------------------- #
GRUB_INIT_TUNE="60 800 1"
GRUB_DISTRIBUTOR="Gentoo"
GRUB_TERMINAL_INPUT=console
GRUB_TERMINAL_OUTPUT=console
GRUB_DISABLE_RECOVERY=false
GRUB_DISABLE_OS_PROBER=true
GRUB_DEVICE_UUID="405360494790545685"
GRUB_DEVICE_BOOT_UUID="c4b9df25-3c29-4d39-89dc-46ee2bb97e8f"
GRUB_DISABLE_LINUX_UUID=false
GRUB_DISABLE_LINUX_PARTUUID=true
# --------------------------------------------------------------------------- #
# GRUB_DEVICE_BOOT_PARTUUID="51c5c3f8-6cb6-4713-8663-a49c99059259"
# --------------------------------------------------------------------------- #
# REF: https://wiki.gentoo.org/wiki/GRUB/Configuration_variables
# REF: https://www.gnu.org/software/grub/manual/grub/html_node/Root-Identifcation-Heuristics.html
# END
# --------------------------------------------------------------------------- #
EOF

# DO NOT install grub, but we will issue grub-update to generate the config
# Generate /boot/grub/grub.cfg from /etc/default/grub vars
#  - note: we don't use a ESP / EFI on PowerNV
#  - we don't need any EFI files in /boot/efi
#  - we don't need any grub modules in /boot/grub/{locale,powerpc-ieee1275}
#  - grub will think we're on a PPC Mac if we run grub-install, so don't bother
# set symlinks for draid drives, since grub2 sucks (when not patched via Chimera scripts)

cat<< EOF > /etc/grub.d/00_zfs-symlinks
#!/bin/sh
# --------------------------------------------------------------------------- #
# Creates symlinks to current ZFS pool devices if they do not exist
# These are required for Grub2, as currently it's not super smart and fails without these enabled
# Date: 2023-10-20
# Version: 1.0.0
# Author: em-winterschon
# ToDo: generate the symlinks programmatically, without hardcoding
# --------------------------------------------------------------------------- #
UUID_ZFS_RPOOL=$(blkid | grep "LABEL=\"rpool\"" | tail -n 1 | awk -F"UUID=" '{print $2}' | awk '{print $1}')
UUID_ZFS_BOOT_DEV=$(blkid | grep "LABEL=\"ZFS_BOOT_EXT\"" | tail -n 1 | awk -F"UUID=" '{print $2}' | awk '{print $1}')
UUID_ZFS_BOOT_PART=$(blkid | grep "LABEL=\"ZFS_BOOT_EXT\"" | tail -n 1 | awk -F"PARTUUID=" '{print $2}' | awk '{print $1}')
# --------------------------------------------------------------------------- #
test -L /dev/draid1:1d:3c:1s-0 || \
  ln -s /dev/disk/by-uuid/$(echo ${UUID_ZFS_RPOOL} | sed 's/"//g') /dev/draid1:1d:3c:1s-0

test -L /dev/ata-SATADOM-SL_3IE3_V2_BCA11709200050517 || \
  ln -s /dev/sda /dev/ata-SATADOM-SL_3IE3_V2_BCA11709200050517

test -L /dev/ata-SATADOM-SL_3IE3_V2_BCA11708020382305 || \
  ln -s /dev/sdb /dev/ata-SATADOM-SL_3IE3_V2_BCA11708020382305

test -L /dev/ata-SATADOM-SL_3IE3_V2_BCA11708020382932 || \
  ln -s /dev/sdc /dev/ata-SATADOM-SL_3IE3_V2_BCA11708020382932
# --------------------------------------------------------------------------- #
EOF
chmod 755 /etc/grub.d/00_zfs-symlinks

# ---------------------------------------------------- #
# testing grub config script
# ---------------------------------------------------- #
/etc/grub.d/00_zfs-symlinks && \
  source /etc/default/grub && \
  grub-probe --target=device /boot
#
# Example Output:
#   /dev/sdd1
#
grub-probe --target=device /
#   /dev/sda1
#   /dev/sdb
#   /dev/sda
#   /dev/sdc

# ---------------------------------------------------- #
# Generating grub config script
# ---------------------------------------------------- #
grub-mkconfig -o /boot/grub/grub.cfg

# Example output:
#    Generating grub configuration file ...
#    Found linux image: /boot/vmlinux-6.1.57-gentoo-dist
#    Found initrd image: /boot/initramfs-6.1.57-gentoo-dist.img
#    done

# ---------------------------------------------------- #
# KERNEL MODS
#   Note: we'll need to tune the ZFS module eventually
# ---------------------------------------------------- #
mkdir /etc/modprobe.d
echo "blacklist hci_vhci" > /etc/modprobe.d/blacklist.bluetooth.conf
echo "blacklist bluetooth" >> /etc/modprobe.d/blacklist.bluetooth.conf
echo "blacklist nouveau" > /etc/modprobe.d/blacklist.nouveau.conf
echo "blacklist cfg80211" > /etc/modprobe.d/blacklist.wifi.conf
echo "blacklist soundcore" > /etc/modprobe.d/blacklist.snd.conf
echo "blacklist snd_pcm" >> /etc/modprobe.d/blacklist.snd.conf
echo "blacklist snd_hda_intel" >> /etc/modprobe.d/blacklist.snd.conf
echo "blacklist snd" >> /etc/modprobe.d/blacklist.snd.conf

# ---------------------------------------------------- #
# FSTAB, Dynamic
#  Example below...
# ---------------------------------------------------- #
# UUID="uuid-here"  /boot              ext4    rw,defaults  0      2
# tmpfs             /var/cache/ccache  tmpfs   rw,noexec,nodev,nosuid,size=12G,uid=0,gid=250,mode=0755  0 2
# tmpfs             /tmp               tmpfs   rw,nodev,nosuid,size=2G  0 2
# ---------------------------------------------------------------------------- #

cat<<EOF >/etc/fstab
# ---------------------------------------------------------------------------- #
# FS Mounts | Dynamically generated during scripted provisioning
# Date: 2023-10-20
# ---------------------------------------------------------------------------- #
# <UUID="" || LABEL="">  <mountpoint>       <type>  <opts>       <dump> <pass>
# ---------------------------------------------------------------------------- #
UUID="${UUID_ZFS_BOOT_DEV}"  /boot  ext4    rw,defaults  0 2
tmpfs             /var/cache/ccache  tmpfs   rw,noexec,nodev,nosuid,size=12G,uid=0,gid=250,mode=0755  0 2
tmpfs             /tmp               tmpfs   rw,nodev,nosuid,size=2G  0 2
EOF

# old method
## echo -e "UUID=${UUID_ZFS_BOOT_PART}\t\t/boot\t\text4\t\trw,defaults\t\t0 2" >> /etc/fstab
## echo -e "tmpfs\t\t/var/cache/ccache\t\ttmpfs\t\trw,noexec,nodev,nosuid,size=12G,uid=0,gid=250,mode=0755\t\t0\t\t2" >> /etc/fstab
## echo -e "tmpfs\t\t/tmp\t\ttmpfs\t\trw,nodev,nosuid,size=2G\t\t0\t\t2" >> /etc/fstab

# ---------------------------------------------------- #
# Network Time
# ---------------------------------------------------- #
mkdir -p /usr/local/etc/chrony
rm /etc/chrony/chrony.conf
touch /usr/local/etc/chrony/chrony.conf
ln -s /usr/local/etc/chrony/chrony.conf /etc/chrony/chrony.conf

cat <<EOF > /usr/local/etc/chrony/chrony.conf
# Chronyd config -- Vernetzen v1.0.0
pool 0.pool.ntp.org iburst minpoll 6 maxpoll 6
pool 1.pool.ntp.org iburst minpoll 6 maxpoll 6
pool 2.pool.ntp.org iburst minpoll 6 maxpoll 6
pool 3.pool.ntp.org iburst minpoll 6 maxpoll 6
driftfile /var/lib/chrony/drift
makestep 1.0 3
rtcsync
hwtimestamp *
minsources 3
#allow 192.168.0.0/16
#local stratum 10
keyfile /etc/chrony.keys
leapsectz right/UTC
logdir /var/log/chrony
log measurements statistics tracking
EOF

# ---------------------------------------------------- #
# SSH Keys
# ---------------------------------------------------- #
echo "PermitRootLogin yes" >> /etc/ssh/sshd_config.d/ppc64-root-permit-temp.conf
ssh-keygen -t ed25519 -f /root/.ssh/id_ed25519 -C "root@$HOSTNAME" -q -N ""
echo "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICGaGcRcmW6vcZqb3lqUHxoHdc5kLf3PKV3RdtBeZdYn eva@null.ed25519" > /root/.ssh/authorized_keys
echo "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFlf1YB1YPxRwtPKDGx+XyktPG+VQGh+cZPLD0nGZ/ei verwalterin@rfc1918.cloud" >> /root/.ssh/authorized_keys
exit

# ---------------------------------------------------- #
# EXIT CHROOT
#   CRITICAL -- unmount ALL gentoo paths
#   then export the pool
# ---------------------------------------------------- #
/usr/bin/df | grep gentoo | awk '{print $6}' | sort -r | xargs umount
/usr/bin/mount | grep gentoo | awk '{print $3}' | sort -r | xargs umount
zpool export rpool

# ---------------------------------------------------- #
# < reboot >
# ---------------------------------------------------- #
```

### Stage 3 - Post-Install
#### De-Verbosing OpenRC
Verbose mode for OpenRC services is useful after the initial install, but is otherwise kinda loud. Remove verbose mode as follows:
```
sed -i 's/rc_verbose=yes/rc_verbose=no/g' /etc/rc.conf
```
#### Tuning ZFS Memory
```
# Optionally set 16GB L1ARC
echo "options zfs zfs_arc_max=17179869184" >> /etc/modprobe.d/zfs.conf
```
#### Emerging Optionals
```
# Portage USE + Accept
mkdir -p /etc/portage/package.{accept_keywords,mask,use}/{app-admin,app-metrics,app-misc,app-shells}
mkdir -p /etc/portage/package.{accept_keywords,mask,use}/{dev-lang,net-fs}
mkdir -p /etc/portage/package.{accept_keywords,mask,use}/{net-libs,net-nds}
mkdir -p /etc/portage/package.{accept_keywords,mask,use}/{sys-apps,sys-block,sys-fs,sys-process}

# Filesystems
emerge net-libs/libiscsi sys-block/open-iscsi sys-fs/multipath-tools sys-fs/mdadm sys-fs/dmraid net-fs/nfs-utils net-nds/rpcbind
rc-update add mdraid boot
rc-update add multipath boot

# Telemetry
emerge app-metrics/ceph_exporter app-metrics/node_exporter app-metrics/portage_exporter app-metrics/process_exporter

# SysEng
emerge app-admin/syslog-ng sys-apps/nvme-cli sys-apps/rng-tools sys-apps/hwloc sys-apps/usbutils sys-apps/ethtool sys-apps/pciutils
emerge sys-process/iotop-c

# DevSys 
emerge sys-process/htop dev-vcs/git app-misc/tmux app-misc/tmuxp app-shells/tmux-bash-completion dev-util/strace app-shells/fish

# GoLang apps
emerge dev-lang/go 
go install github.com/owenthereal/ccat@latest

```

#### MAYBE MAYBE Itemizing the Root Pool
```
## We may return and add these after installation
# These might have been causing issues on the last install iteration during boot 
zfs create -o canmount=off rpool/opt
zfs create -o canmount=off rpool/usr
zfs create -o canmount=off rpool/usr/local
zfs create -o canmount=off rpool/var
zfs create -o canmount=off rpool/var/lib
zfs create -o canmount=off rpool/var/tmp
zfs create -o canmount=off rpool/home
zfs create -o canmount=off rpool/root
```

#### Enabling and Fixing the Fish Shell
```
emerge app-shells/fish

# fix ZFS auto-complete for ZFS
mv /usr/share/fish/functions/__fish_is_zfs_feature_enabled.fish /usr/share/fish/functions/__fish_is_zfs_feature_enabled.fish__broken_v3.6.1
cat<<EOF > /usr/share/fish/functions/__fish_is_zfs_feature_enabled.fish
function __fish_is_zfs_feature_enabled -a feature target -d "Returns 0 if the given ZFS feature is available or enabled"
    type -q zpool
    or return
    set -l pool (string replace -r '/.*' '' -- $target)
    set -l feature_name ""
    if test -z "$pool"
        set feature_name (zpool get -H all 2>/dev/null | string match -r "\s$feature\s")
    else
        set feature_name (zpool get -H all $pool 2>/dev/null | string match -r "$pool\s$feature\s")
    end
    if test $status -ne 0 # No such feature
        return 1
    end
    set -l state (echo $feature_name | cut -f3)
    string match -qr '(active|enabled)' -- $state
    return $status
end
EOF
```

#### Dev Environment  Additional Packages
Additional packages to install - names not necessarily complete
```
- parallel
- clang
- llvm
- python
- python-pip
- python-devel
- ncurses
- smartctl
- exporters
- ipmitool
- exa / eza
- netstat
- ifconfig
```

## Virtualization on PowerNV

### Preparing NVMe Storage
```
# for i in {0..3}; do wipefs -a /dev/nvme${i}n1; done
cat<<EOF > /tmp/zfs-prep-nvme.part
label: gpt
label-id: A5F40A25-EF5D-4132-8A9F-A555CEEE8F29
device: /dev/nvme0n1
unit: sectors
first-lba: 2048
last-lba: 976773134
sector-size: 512
EOF

# for i in {0..3}; do sfdisk /dev/nvme${i}n1 < /tmp/zfs-prep-nvme.part; done
# ls -al /dev/disk/by-*/ | grep nvme

POOL="optane-aics-ppc"

zpool create -O mountpoint=none \
    -O compression=zstd \
    -O relatime=on \
    -O xattr=sa \
    -O acltype=posixacl \
    -o autoreplace=on \
    -o ashift=12 \
    -o autotrim=on \
    -o listsnapshots=on \
    -m none \
    ${POOL} draid1:2d:4c:0s \
        <4x NVMe drives go here>

zfs create \
        -o atime=off \
        -o relatime=on \
        -o canmount=noauto \
        -o aclinherit=passthrough \
        -o aclmode=passthrough \
        -o utf8only=on \
        -o recordsize=256K \
        -o compression=zstd \
        -o dedup=sha256,verify \
        -o reservation=256M \
        -o mountpoint=/opt/storage/local/zfs/datastore \
        ${POOL}/datastore

zfs create \
        -o atime=off \
        -o relatime=on \
        -o canmount=off \
        -o mountpoint=none \
        ${POOL}/gentoo

zfs create \
        -o atime=off \
        -o relatime=on \
        -o canmount=on \
        -o aclinherit=passthrough \
        -o aclmode=passthrough \
        -o utf8only=on \
        -o recordsize=256K \
        -o compression=zstd-12 \
        -o checksum=sha512 \
        -o dedup=sha256,verify \
        -o reservation=256M \
        -o mountpoint=/srv/gentoo/repos \
        ${POOL}/gentoo/repos
```

### Creating a Local Package Repository
- https://wiki.gentoo.org/wiki/Eselect/Repository
- https://wiki.gentoo.org/wiki/Creating_an_ebuild_repository
```
useradd --uid 3072 --gid 3072 --home-dir /home/gen2repo --create-home --groups portage gen2repo
emerge --ask app-eselect/eselect-repository
eselect repository create rfc1918.dev.gen2repo-ppc64le /srv/gentoo/repos/rfc1918.dev.gen2repo-ppc64le
sed -i 's/[rfc1918.dev.gen2repo-ppc64le]/[rfc1918-dev-gen2repo-ppc64le]/g' /etc/portage/repos.conf/eselect-repo.conf

chown -R gen2repo:gen2repo /srv/gentoo/repos
chmod g+s /srv/gentoo/repos
chmod u+s /srv/gentoo/repos
chmod g+s /srv/gentoo/repos/rfc1918.dev.gen2repo-ppc64le
chmod u+s /srv/gentoo/repos/rfc1918.dev.gen2repo-ppc64le

cd /srv/gentoo/repos/rfc1918.dev.gen2repo-ppc64le && \
  git clone git@gitlab.com:rfc1918.dev/pub/gen2repo-ppc64le.git . \

mkdir repo-conf   
cat<<EOF >/srv/gentoo/repos/rfc1918.dev.gen2repo-ppc64le/repo-conf/eselect-repo.conf
[rfc1918-dev-gen2repo-ppc64le]
location = /srv/gentoo/repos/rfc1918.dev.gen2repo-ppc64le
sync-type = git
sync-uri = git@gitlab.com:rfc1918.dev/pub/gen2repo-ppc64le.git
priority = 100
EOF

# there's a whole thing about Gitlab SSH Deploy keys ... 
git add .
git commit -a -m "initial commit"
  
```

### Installing the QEMU Hypervisor
- Reference: https://www.qemu.org/docs/master/system/ppc/powernv.html
```
mkdir -p /etc/portage/package.use/app-emulation
mkdir -p /etc/portage/package.accept_keywords/app-emulation

# install any version, even unstable 
# note: all libvirt versions @ <=9.8.0 on ppc64 are unstable
cat<< EOF >/etc/portage/package.accept_keywords/app-emulation/libvirt
<app-emulation/libvirt-9999 ~ppc64
EOF

cat<< EOF >/etc/portage/package.use/app-emulation/libvirt
app-emulation/libvirt caps virt-network
EOF

# Select only stable qemu versions
cat<< EOF >/etc/portage/package.accept_keywords/app-emulation/qemu
<app-emulation/qemu-9999 ppc64
EOF

cat<< EOF >/etc/portage/package.use/app-emulation/qemu
app-emulation/qemu infiniband -alsa iscsi ncurses numa -pipewire -pulseaudio -spice -usb -vnc xattr -xen
EOF

# we want the latest kernel + qemu + libvirt
emerge --autounmask-write --tree --deep --verbose --newuse --update sys-kernel/vanilla-kernel
emerge --autounmask-write --tree --deep --verbose --newuse --update app-emulation/qemu app-emulation/libvirt

# services
rc-update add libvirtd
rc-service libvirtd start

rc-update add libvirt-guests
rc-service libvirt-guests start
```

## Appendix A
### LVM Raid for SATADOMs
```

» pvcreate -v /dev/sd[a-c][1-2]
» pvs
  PV         VG Fmt  Attr PSize PFree
  /dev/sda1     lvm2 ---  4.00g 4.00g
  /dev/sda2     lvm2 ---  4.00g 4.00g
  /dev/sdb1     lvm2 ---  4.00g 4.00g
  /dev/sdb2     lvm2 ---  4.00g 4.00g
  /dev/sdc1     lvm2 ---  4.00g 4.00g
  /dev/sdc2     lvm2 ---  4.00g 4.00g

» vgcreate -f -v satadoms /dev/sd[a-c][1]
» vgcreate -f -v swaps /dev/sd[a-c][2]
» vgs
  VG       #PV #LV #SN Attr   VSize   VFree
  satadoms   3   0   0 wz--n- <11.99g <11.99g
  swaps      3   0   0 wz--n- <11.99g <11.99g

» modprobe dm-raid raid1

» lsmod | grep dm
dm_raid                48044  0
raid456               186816  1 dm_raid
md_mod                185400  2 dm_raid,raid456
dm_mod                174436  1 dm_raid

» lvcreate --type raid1 -L 3.8G -n boot satadoms
Rounding up size to full physical extent 3.80 GiB
Logical volume "boot" created.

» lvcreate --type raid1 -L 3.8G -n swap swaps
  Rounding up size to full physical extent 3.80 GiB
  Logical volume "swap" created.

» lvs
  LV   VG       Attr       LSize Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  boot satadoms rwi-a-r--- 3.80g                                    61.94
  swap swaps    rwi-a-r--- 3.80g                                    0.98  
  
» echo -e "dm_raid\nraid1" >> /etc/initramfs-tools/modules
» update-initramfs -c -k all

» mkfs.ext4 /dev/mapper/satadoms-boot
Creating filesystem with 498688 4k blocks and 124672 inodes
Filesystem UUID: a1196ab9-998f-4b7f-8d5e-234d5fc22256
...
filesystem accounting information: done

» mkswap  /dev/mapper/swaps-swap
Setting up swapspace version 1, size = 3.8 GiB (4081053696 bytes)
no label, UUID=a54db742-61c7-4106-802c-e17d70895add

» pvs && vgs && lvs
```